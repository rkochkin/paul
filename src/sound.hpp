#pragma once
#include <SDL.h>

class Sound
{
public:
    Sound();
    ~Sound();
    int bump();
private:
    // load WAV file
    SDL_AudioSpec wavSpecBump;
    Uint32 wavLength;
    Uint8 *wavBuffer;

    SDL_AudioDeviceID deviceId;
};
