#include "sound.hpp"
#include <stdexcept>

Sound::Sound(){

    if (SDL_LoadWAV("/home/ksy/me/git/paul/src/res/bump.wav", &wavSpecBump, &wavBuffer, &wavLength)==nullptr){
        throw std::runtime_error(std::string("SDL_LoadWAV()") + SDL_GetError()) ;
    };
    // open audio device
    deviceId = SDL_OpenAudioDevice(NULL, 0, &wavSpecBump, NULL, 0);
}

int Sound::bump(){
    // play audio
    SDL_ClearQueuedAudio(deviceId);
    int success = SDL_QueueAudio(deviceId, wavBuffer, wavLength);
    SDL_PauseAudioDevice(deviceId, 0);
    return success;
}

Sound::~Sound(){

}


