#pragma once
#include "bird.hpp"
#include "sound.hpp"
#include <SDL.h>

class Game
{
public:
    Game(SDL_Renderer *renderer);
    bool tick(int fx, int fy);
    int exec();

    ~Game();
private:
    SDL_Renderer *renderer;
    Sound sound;
    Bird bird;
};
