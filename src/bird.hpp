#pragma once
#include <SDL.h>
#include "application.hpp"
#include "sound.hpp"

class Bird
{
public:
	Bird(SDL_Renderer* renderer, Sound* sound);
	~Bird();
	void draw();
	void tick(int fx, int fy, int rx, int ry);
private:
	SDL_Renderer* renderer;
	SDL_Texture *birdUp;
	SDL_Texture *birdDown;
	Sound *sound;
	float x = Application::Width  / 4;
	float y = Application::Height / 2;
	float vx = 0.0f;
	float vy = 0.0f;
};
