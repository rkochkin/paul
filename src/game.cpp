#include "game.hpp"

Game::Game(SDL_Renderer* renderer):
    renderer(renderer),
    sound(),
    bird(renderer, &sound)
{

}

Game::~Game(){

}

bool Game::tick(int fx, int fy){
    bird.tick(fx, fy, 0, 0);
    return true;
}
//SDL_SCANCODE_UP SDLK_UP

int Game::exec(){
    auto oldTick = SDL_GetTicks();
    int fx=0;
    int fy=0;
    for (auto done = false; !done;)
    {
        SDL_Event e;
        const Uint8 *keystate = SDL_GetKeyboardState(NULL);
        if (SDL_PollEvent(&e)){
            switch (e.type)
            {
                case SDL_KEYDOWN:
                case SDL_KEYUP:
                    fx = fy = 0;
                    if (keystate[SDL_SCANCODE_UP]) fy = 1;
                    if (keystate[SDL_SCANCODE_DOWN]) fy = -1;
                    if (keystate[SDL_SCANCODE_RIGHT]) fx = 1;
                    if (keystate[SDL_SCANCODE_LEFT]) fx = -1;
                break;
                case SDL_QUIT:
                    done = true;
                break;
            }
        }
        SDL_Delay(5);
        auto currentTick = SDL_GetTicks();
        for (auto i = oldTick; i < currentTick; i++){
            if (!tick(fx, fy)){
                return 1;
            }
        }
        oldTick = currentTick;
        SDL_SetRenderDrawColor(renderer, 0x00, 0xff, 0xff, 0xff);
        SDL_RenderClear(renderer);
        bird.draw();
        SDL_RenderPresent(renderer);
    }
    return 0;
}