#pragma once

class IBody{
public:
  virtual double getX() = 0;
  virtual void setX(double x) = 0;
  virtual double getY() = 0;
  virtual void setY(double y) = 0;
  virtual double getRot() = 0;
  virtual void setRot(double) = 0;
};