#include "application.hpp"
#include <stdexcept>
#include "game.hpp"

Application::Application(){
	auto res = SDL_Init(SDL_INIT_EVERYTHING);
	if (res != 0){
		throw std::runtime_error(std::string("SDL_Init(SDL_INIT_EVERYTHING)") + SDL_GetError()) ;
	}
	SDL_CreateWindowAndRenderer(Width, Height, 0, &window, &renderer); //SDL_WINDOW_BORDERLESS
	SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);
}

int Application::exec(){
	Game g(renderer);
	g.exec();

	return 0;
}
