#include "application.hpp"
#include <SDL.h>

int main(int argc, char const *argv[])
{
	Application app;
	return app.exec();
}
