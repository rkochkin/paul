#pragma once

#include <SDL.h>

class Application
{
public:
	static const auto Width = 800;
	static const auto Height = 600;
	Application();
	int exec();
private:
	SDL_Window *window;
	SDL_Renderer *renderer;
};
