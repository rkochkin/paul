#include "bird.hpp"
#include "load_texture.hpp"
#include <stdexcept>


Bird::Bird(SDL_Renderer* renderer, Sound* sound):
    renderer(renderer),
    birdUp(loadTexture(renderer, "/home/ksy/me/git/paul/src/res/bird_up.bmp")),
    birdDown(loadTexture(renderer, "/home/ksy/me/git/paul/src/res/bird_down.bmp")),
    sound(sound)
{
}

Bird::~Bird(){
    SDL_DestroyTexture(birdUp);
    SDL_DestroyTexture(birdDown);
}

void Bird::draw(){
    SDL_Rect r;
    r.x = x -64;
    r.y = y -64;
    r.w = 128;
    r.h = 128;
    auto res = SDL_RenderCopyEx(renderer, SDL_GetTicks() % 300> 150 ? birdUp : birdDown, nullptr, &r, vy * 50, nullptr, SDL_FLIP_NONE);
    if (res != 0){
        throw std::runtime_error(std::string("SDL_RenderCopy") + SDL_GetError());
    }
}

void Bird::tick(int fx, int fy, int rx, int ry){
    vy -= fy > 0 ? 0.003 : 0;
    vy += 0.001;
    y += vy;
    if (y < 0) {
        y = 0;
        vy=0;
    }
    if (y> Application::Height){
        y= Application::Height;
        if (vy>0.05)sound->bump();
        vy= -vy * 0.8;
    }
    vx += fx * 0.003;
    x += vx;
    if (x < 0) {
        x = 0;
        vx= -vx*0.3;
    }
    if (x> Application::Width){
        x= Application::Width;
        vx= -vx*0.3;
        sound->bump();
    }
    vx *= 0.9995;
    vy *= 0.9995;
}
